/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author vanshika bhavnani
 */
public class MySQLConnect {
    public static Connection getConnection(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_mgmt", "vanshika", "vanshika");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Connection Failed " + e.getMessage());
            return null;
        }
    }
    public static void main(String args[]){
        Connection conn = getConnection();
        if(conn != null){
            JOptionPane.showMessageDialog(null, "Connection Established");
        }
    }
}
